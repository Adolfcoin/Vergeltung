//17copyright   

                                                

import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    id: button
    height: 37
    property string shadowPressedColor
    property string shadowReleasedColor
    property string pressedColor
    property string releasedColor
    property string icon: ""
    property string textColor: "#FFFFFF"
    property int fontSize: 12
    property alias text: label.text
    signal clicked()

    // Dynamic label width
    Layout.minimumWidth: (label.contentWidth > 80)? label.contentWidth + 20 : 100

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height - 1
        y: buttonArea.pressed ? 0 : 1
        //radius: 4
        color: {
            parent.enabled ? (buttonArea.pressed ? parent.shadowPressedColor : parent.shadowReleasedColor)
                           : Qt.lighter(parent.shadowReleasedColor)
        }
        border.color: Qt.darker(parent.releasedColor)
        border.width: parent.focus ? 1 : 0

    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height - 1
        y: buttonArea.pressed ? 1 : 0
        color: {
            parent.enabled ? (buttonArea.pressed ? parent.pressedColor : parent.releasedColor)
                           : Qt.lighter(parent.releasedColor)

        }
        //radius: 4


    }

    Text {
        id: label
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        font.family: "Arial"
        font.bold: true
        font.pixelSize: button.fontSize
        color: parent.textColor
        visible: parent.icon === ""
//        font.capitalization : Font.Capitalize
    }

    Image {
        anchors.centerIn: parent
        visible: parent.icon !== ""
        source: parent.icon
    }

    MouseArea {
        id: buttonArea
        anchors.fill: parent
        onClicked: parent.clicked()
    }

    Keys.onSpacePressed: clicked()
    Keys.onReturnPressed: clicked()
}
