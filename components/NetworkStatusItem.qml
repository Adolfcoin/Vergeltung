//17copyright   

                                                

import QtQuick 2.0
import moneroComponents.Wallet 1.0

Row {
    id: item
    property var connected: Wallet.ConnectionStatus_Disconnected

    function getConnectionStatusImage(status) {
        if (status == Wallet.ConnectionStatus_Connected)
            return "../images/statusConnected.png"
        else
            return "../images/statusDisconnected.png"
    }

    function getConnectionStatusColor(status) {
        if (status == Wallet.ConnectionStatus_Connected)
            return "#E60000"
        else
            return "#AAAAAA"
    }

    function getConnectionStatusString(status) {
        if (status == Wallet.ConnectionStatus_Connected) {
            if(!appWindow.daemonSynced)
                return qsTr("Synchronizing")
            return qsTr("Connected")
        }
        if (status == Wallet.ConnectionStatus_WrongVersion)
            return qsTr("Wrong version")
        if (status == Wallet.ConnectionStatus_Disconnected)
            return qsTr("Disconnected")
        return qsTr("Invalid connection status")
    }

    Item {
        id: iconItem
        anchors.bottom: parent.bottom
        width: 50
        height: 50

        Image {
            anchors.centerIn: parent
            source: getConnectionStatusImage(item.connected)
        }
    }

    Column {
        anchors.bottom: parent.bottom
        height: 53
        spacing: 3

        Text {
            anchors.left: parent.left
            font.family: "Arial"
            font.pixelSize: 12
            color: "#545454"
            text: qsTr("Network status") + translationManager.emptyString
        }

        Text {
            anchors.left: parent.left
            font.family: "Arial"
            font.pixelSize: 18
            color: getConnectionStatusColor(item.connected)
            text: getConnectionStatusString(item.connected) + translationManager.emptyString
        }
    }
}
