//17copyright   

 

#include "gtest/gtest.h"

#include "include_base_utils.h"

int main(int argc, char** argv)
{
  epee::string_tools::set_module_name_and_folder(argv[0]);
  mlog_configure(mlog_get_default_log_path("unit_tests.log"), true);
  epee::debug::get_set_enable_assert(true, false);

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
