//17copyright   

 
#pragma once

#include <boost/optional/optional.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <cstddef>
#include <functional>
#include <thread>
#include <utility>
#include <vector>

namespace tools 
{
//! Manages zero or more threads for work dispatching.
class thread_group
{
public:

  //! \return `get_max_concurrency() ? get_max_concurrency() - 1 : 0`
  static std::size_t optimal();

  //! \return `count ? min(count - 1, optimal()) : 0`
  static std::size_t optimal_with_max(std::size_t count);

  //! Create an optimal number of threads.
  explicit thread_group() : thread_group(optimal()) {}

  //! Create exactly `count` threads.
  explicit thread_group(std::size_t count);

  thread_group(thread_group const&) = delete;
  thread_group(thread_group&&) = delete;

  //! Joins threads, but does not necessarily run all dispatched functions.
  ~thread_group() = default;

  thread_group& operator=(thread_group const&) = delete;
  thread_group& operator=(thread_group&&) = delete;

  //! \return Number of threads owned by `this` group.
  std::size_t count() const noexcept {
    if (internal) {
      return internal->count();
    }
    return 0;
  }

  //! \return True iff a function was available and executed (on `this_thread`).
  bool try_run_one() noexcept {
    if (internal) {
      return internal->try_run_one();
    }
    return false;
  }

  /*! `f` is invoked immediately if `count() == 0`, otherwise execution of `f`
  is queued for next available thread. If `f` is queued, any exception leaving
  that function will result in process termination. Use std::packaged_task if
  exceptions need to be handled. */
  template<typename F>
  void dispatch(F&& f) {
    if (internal) {
      internal->dispatch(std::forward<F>(f));
    }
    else {
      f();
    }
  }

private:
  class data {
  public:
    data(std::size_t count);
    ~data() noexcept;

    std::size_t count() const noexcept {
      return threads.size();
    }

    bool try_run_one() noexcept;
    void dispatch(std::function<void()> f);

  private:
    struct work;

    struct node {
      std::unique_ptr<work> ptr;
    };

    struct work {
      std::function<void()> f;
      node next;
    };

    //! Requires lock on `mutex`.
    std::unique_ptr<work> get_next() noexcept;

    //! Blocks until destructor is invoked, only call from thread.
    void run() noexcept;

  private:
    std::vector<boost::thread> threads;
    node head;
    node* last;
    boost::condition_variable has_work;
    boost::mutex mutex;
    bool stop;
  };

private:
  // optionally construct elements, without separate heap allocation
  boost::optional<data> internal;
};

}
